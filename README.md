# D3Brush Frelard

This project is a D3Brush tableau add-in using frelard.
The add-in can be used to display dashboard's data corresponding to the selected date interval.

### Pre-requisites
* Tableau Desktop PreRelease version 17.0914.2200 or above.
* You must have Node.js or Python installed (or other software which can serve the contents of this folder over http). You can get Node.js from http://nodejs.org and Python from https://www.python.org/downloads/

### Setup
1. Copy the `.trex` files from `.\Extensions` to `~\Documents\My Tableau Repository (Beta)\Extensions` so they are available to Tableau.
2. Open a command prompt window to the location where you cloned this repo.
3. Start a simple file hosting server:
  * Python 2.x : `python -m SimpleHTTPServer 8766`
  * Python 3.x : `python -m http.server 8766`
  * Node.js : First run `npm install http-server -g` (only the first time) then `http-server -p 8766`
4. Launch Tableau and use the add-in on a Dashboard which has date data

### Custom Date/Time formats

You can change the list of known date / time formats in the file 'd3brush_handler.js'

```
/**
 * Attempts to parse a date using D3's timeParse, or fall back to other strategies
 */
let parseDate = (function(){

  // [...]

  // Generates a parser for each knwon time-format.
  // These parsers should return a Date or null in
  // case of bad format.
  //
  let knownFormats = [
    d3.timeParse("%Y-%m-%d %H:%M:%S"),
    // ... add any known time formats here
    // d3.timeParse("%Y-%m-%d %H:%M:%S"),

    // As a last fallback, try the JS default format
    parseDate__jsNative
  ];


  // [...]

})();
```



### Usage

After the add-in is loaded, on the first start you can select the fields for the D3Brush from dropdown boxes. This setting is saved in the tableau workbook after pressing the ok button.

If the add-in reloads it uses the previously saved settings.

With removing and re-adding the add-in to the workbook the settings selector shows again.

### Credits

Original work by [Mike Bostock](https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172) under GNU GPLv3, Tableau integration by [Chris DeMartini](https://twitter.com/demartsc)
