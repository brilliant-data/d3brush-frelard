'use strict';
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function selectXAxis() {
  document.getElementById("xAxisDropdown").classList.toggle("show");
}

function selectYAxis() {
  document.getElementById("yAxisDropdown").classList.toggle("show");
}

function selectSheet() {
	document.getElementById("sheetSelectorDropdown").classList.toggle("show");
}

// Wrap everything in an anonymous function to avoid polluting the global namespace
(function () {
	var sheets;
	var xAxisField;
	var yAxisField;
	var selectedSheet;
	var worksheetId;
	var chartColor;
	var chartBackgroundColor;
	var vizBackgroundColor;
	var xDropdown;
	var yDropdown;

	/**
	* Initializes the UI elements of the configure dialog
	*/
	function init() {

		getSettings();

		$('#closeButton').click(closeDialog);
		initDropdowns();
		initColorPickers();

	}

	function initColorPickers() {
		var chartColorPicker = $('#chartColor');
		var chartBackgroundColorPicker = $('#chartBackgroundColor')
		var vizBackgroundColorPicker = $('#vizBackgroundColor')

		chartColor = tableau.extensions.settings.get("chartColor") || chartColorPicker.val();
		chartBackgroundColor = tableau.extensions.settings.get("chartBackgroundColor") || chartBackgroundColorPicker.val();
		vizBackgroundColor = tableau.extensions.settings.get("vizBackgroundColor") || vizBackgroundColorPicker.val();

		if (chartColor) {
			chartColorPicker.val(chartColor);
		}
		chartColorPicker.on('change input', function(e){
			chartColor = e.target.value;
		});

		if (chartBackgroundColor) {
			chartBackgroundColorPicker.val(chartBackgroundColor)
		}
		chartBackgroundColorPicker.on('change input', function(e){
			chartBackgroundColor = e.target.value;
		});

		if (vizBackgroundColor) {
			vizBackgroundColorPicker.val(vizBackgroundColor)
		}
		vizBackgroundColorPicker.on('change input', function(e){
			vizBackgroundColor = e.target.value;
			console.log("ABO " + vizBackgroundColor)
		});
	}
	  
	function addOnClickListener(){
		
			$('#xAxisDropdown a').on('click', (function(){
				xAxisField = $(this).text();
				$("#xAxisButton > span").text(xAxisField);				 
			}));
			 
			$('#yAxisDropdown a').on('click', (function(){
				yAxisField = $(this).text();
				$("#yAxisButton > span").text(yAxisField);
				 
			}));
	}

	function initDropdowns(){
		sheets = tableau.extensions.dashboardContent.dashboard.worksheets;
		xDropdown = $("#xAxisDropdown");
		yDropdown = $("#yAxisDropdown");
				
		setSheetSelector(sheets);

		if (worksheetId) {
			$("#sheetSelectorButton > span").text(sheets[worksheetId].name);
			getColumns(worksheetId);
		}

		if (xAxisField) {
			$("#xAxisButton > span").text(xAxisField);
		}

		if (yAxisField) {
			$("#yAxisButton > span").text(yAxisField);
		}
	}

	function setSheetSelector(sheets) {
		sheets.map((worksheet, i) => {
			var a = $('<a />');
			a.text(worksheet.name);
			a.attr("worksheet", i);

			$("#sheetSelectorDropdown").append(a);
		});

		addSheetSelectorOnClickListener();
	}

	function addSheetSelectorOnClickListener() {
		$('#sheetSelectorDropdown a').on('click', (function(){
			selectedSheet = $(this).text();
			$("#sheetSelectorButton > span").text(selectedSheet);
			worksheetId = $(this).attr('worksheet');
			xAxisField = ""
			yAxisField = ""
			$("#xAxisDropdown").empty();
			$("#yAxisDropdown").empty();
			$("#xAxisButton > span").text(xAxisField);
			$("#yAxisButton > span").text(yAxisField);
			getColumns(worksheetId);
			selectSheet();
		}));
	}


	function getColumns(i){
		sheets[i].getSummaryDataAsync().then(function (data) {
			data.columns.forEach(function (d, j) {
				var a = $('<a />');
				a.text(d.fieldName);
				a.attr("worksheet",i);
				
				switch(getType(data.data[0][j].value)){
					case("Numeric"):	
						$("#yAxisDropdown").append(a);
						break;
					case("Date"):
						$("#xAxisDropdown").append(a);
						break;
					};
			});

			if ($("#xAxisDropdown").children().length === 0) {
				var a = $('<a />');
				a.text("No available date measure");
				a.attr("worksheet", i);
				a.attr("class", "disabled");
				$("#xAxisDropdown").append(a);
			}

			if ($("#yAxisDropdown").children().length === 0) {
				var a = $('<a />');
				a.text("No available numeric value");
				a.attr("worksheet", i);
				a.attr("class", "disabled");
				$("#yAxisDropdown").append(a);
			}

			addOnClickListener();		
		});
	}

	function getType(data) {
		if(!isNaN(Number(data))){
			return "Numeric";
		}
		if(!data.match(/[a-zA-Z]/i) && !isNaN(Date.parse(data))){
			return "Date";
		}
		return "String";
	}

	function saveSettings(){
		
		tableau.extensions.settings.set("xAxisSetting",xAxisField);
		tableau.extensions.settings.set("yAxisSetting",yAxisField);
		tableau.extensions.settings.set("sheetIdSetting",worksheetId);
		tableau.extensions.settings.set("chartColor",chartColor);
		tableau.extensions.settings.set("chartBackgroundColor",chartBackgroundColor);
		tableau.extensions.settings.set("vizBackgroundColor", vizBackgroundColor);
	}

	function getSettings(){
		xAxisField = tableau.extensions.settings.get("xAxisSetting");
		yAxisField = tableau.extensions.settings.get("yAxisSetting");
		worksheetId = tableau.extensions.settings.get("sheetIdSetting");
		chartColor = tableau.extensions.settings.get("chartColor");
		chartBackgroundColor = tableau.extensions.settings.get("chartBackgroundColor");
		vizBackgroundColor = tableau.extensions.settings.get("vizBackgroundColor");
	}

	$(document).ready(function () {
		tableau.extensions.initializeDialogAsync()
		.then(function (openPayload) {
			console.log("initializeDialogAsync");
			init();
		});
	});

	/**
	 * Stores the selected datasource IDs in the extension settings,
	 * closes the dialog, and sends a payload back to the parent.
	 */
	function closeDialog() {
		saveSettings();
		
		console.log(tableau.extensions.settings);
		tableau.extensions.settings.saveAsync().then((newSavedSettings) => {
			tableau.extensions.ui.closeDialog("true");
		});
	}

})();
