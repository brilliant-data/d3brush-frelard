var svg, margin2, width, height2, x2, y2, xAxis2, brush, area2, context, chartData;

window.addEventListener('resize', function() {
    drawBrush(chartData)
}, true);

function initBrush() {
    svg = d3.select("svg");

    svg.selectAll("*").remove();

    svg.attr("width", window.innerWidth);
    svg.style("background-color", tableau.extensions.settings.get("chartBackgroundColor"));
    $('body').css("background-color", tableau.extensions.settings.get("vizBackgroundColor"));

    margin2 = { top: 15, right: 40, bottom: 30, left: 15 },
    width = +svg.attr("width") - margin2.left - margin2.right,
    height2 = +svg.attr("height") - margin2.top - margin2.bottom;

	// parseDate = d3.timeParse("%Y-%m-%d %H:%M:%S");

	x2 = d3.scaleTime().range([0, width]),
    y2 = d3.scaleLinear().range([height2, 0]);

	xAxis2 = d3.axisBottom(x2);

	brush = d3.brushX()
    .extent([[0, 0], [width, height2]])
    .on("end", brushed);

	area2 = d3.area()
    .curve(d3.curveLinear)
    .x(function (d) { ; return x2(new Date(d.date)); })
    .y0(height2)
    .y1(function (d) { return y2(+d.price); });


	context = svg.append("g")
    .attr("class", "context")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
}




/**
 * Attempts to parse a date using D3's timeParse, or fall back to other strategies
 */
let parseDate = (function(){

  // Attempt to parse as regular Date
  function parseDate__jsNative(d) {
    let ts = Date.parse(d);
    return isNaN(ts) ? null : new Date(Date.parse(d));
  }


  // Generates a parser for each knwon time-format.
  // These parsers should return a Date or null in
  // case of bad format.
  //
  let knownFormats = [
    d3.timeParse("%Y-%m-%d %H:%M:%S"),
    // ... add any known time formats here
    //d3.timeParse("%Y-%m-%d %H:%M:%S"),

    // As a last fallback, try the JS default format
    parseDate__jsNative
  ];



  return function parseDate__Impl(d) {
    for (let i = 0; i < knownFormats.length; ++i) {
      let parsed = knownFormats[i](d);
      if (parsed) {
        return parsed;
      }
    }

    // log the error
    console.log("[ERROR] Unknown Time format encountered: " + d);

    // return a null
    // TODO: Should we throw here or swallow the error and keep on truckin'?
    return null;
  }
})();



function property(v) {
    return function (_) {
        if (!arguments.length)
            return v;
        v = _;
        return this;
    };
}

function drawBrush(tableauData) {
    chartData = tableauData

	initBrush();
    var d3Data = new Array;
    for (j = 0, len = tableauData.length; j < len; j++) {
        d3Data[j] = {
            date: parseDate(tableauData[j][xAxisField]),
            price: parseFloat(tableauData[j][yAxisField]),
        }
    }

    x2.domain(d3.extent(d3Data, function (d) { return new Date(d.date); }));
    y2.domain([0, d3.max(d3Data, function (d) { return +d.price; })]);

    context.append("path")
        .datum(d3Data)
        .attr("class", "area")
        .style("fill", tableau.extensions.settings.get("chartColor"))
        .attr("d", area2);

    context.append("g")
        .attr("class", "axis axis--x")
        .attr("font-family", "sans-serif")
        .attr("font-size", 18)
        .attr("transform", "translate(0," + height2 + ")")
        .style("stroke", "#333")
        .call(xAxis2);

    context.append("g")
        .attr("class", "brush")
        .call(brush)
        .call(brush.move, x2.range());
}

function brushed() {
    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return;
    var s = d3.event.selection || x2.range();
    var filts = s.map(x2.invert, x2);

    sheet = tableau.extensions.dashboardContent.dashboard;
    if (sheet.sheetType === 'worksheet') {
        sheet.applyRangeFilterAsync(xAxisField, { min: filts[0], max: filts[1] }, tableau.FilterUpdateType.Replace);
    }
    else {
        worksheetArray = sheet.worksheets;
        for (var i = 0; i < worksheetArray.length; i++) {
            worksheetArray[i].applyRangeFilterAsync(xAxisField, { min: filts[0], max: filts[1] });
        }
    }
}
