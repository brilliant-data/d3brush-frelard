var brushDataList = [];
var xAxisField;
var yAxisField;
var worksheetId;

// This function will call first after loading
// Initialize Tableau Embed view
function initViz() {

	tableau.extensions.initializeAsync({'configure': configure}).then(function() {
		
		getSettings();
		
		if(xAxisField === undefined || yAxisField === undefined || worksheetId === undefined){
			configure();
		}
		else{
			$("svg").empty();
			initD3Brush();
		}
		
	});
        
}

function configure() {
	const popupUrl = `${window.location.href}/../configure.html`;
	const popupOpts = {
		height: 650,
		width: 550,
	};

	tableau.extensions.ui.displayDialogAsync(popupUrl, "", popupOpts).then((closePayload) => {
		 
		getSettings();
		
		$("svg").empty();
		initD3Brush();

	}).catch(function(error) {
		  // One expected error condition is when the popup is closed by the user (meaning the user
		  // clicks the 'X' in the top right of the dialog).  This can be checked for like so:
		  switch(error.errorCode) {
			case tableau.ErrorCodes.DialogClosedByUser:
				console.log("closed by user")
			  break;
			default:
			  console.error(error.message);
		  }
	});

}


function getSettings() {
	xAxisField = tableau.extensions.settings.get("xAxisSetting");
	yAxisField = tableau.extensions.settings.get("yAxisSetting");
	worksheetId = tableau.extensions.settings.get("sheetIdSetting");
	chartColor = tableau.extensions.settings.get("chartColor");
	backgroundColor = tableau.extensions.settings.get("backgroundColor");
}


function initD3Brush() {
	brushDataList = [];
	
	brushDataList.push({
       xAxisField: xAxisField,
       yAxisField: yAxisField,
       validSheetIndex: worksheetId
    });

	return appApi.initEditor();
}
