// this code was adapted from an original piece of code provided by Tamas Foldi. 
var TABLEAU_NULL, convertRowToObject, errorWrapped, getColumnIndexes, initEditor,
    slice = [].slice,
    interval,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

var parms = new Array;


TABLEAU_NULL = '%null%';
var xAxisField, yAxisField;

errorWrapped = function(context, fn) {
    return function() {
        var args, err, error;
        args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
        try {
            return fn.apply(null, args);
        } catch (error) {
            err = error;
            return console.error("Got error during '", context, "' : ", err.message, err.stack);
        }
    };
};

getColumnIndexes = function(table, required_keys) {
    var c, colIdxMaps, fn, j, len, ref;
    colIdxMaps = {};

    ref = table.columns;

    for (j = 0, len = ref.length; j < len; j++) {
        c = ref[j];
        fn = c.fieldName;

        if (indexOf.call(required_keys, fn) >= 0) {
            colIdxMaps[fn] = c.index;
        }
    }

    return colIdxMaps;
};

convertRowToObject = function(row, attrs_map) {
    var id, name, o;
    o = {};

    for (name in attrs_map) {
        id = attrs_map[name];
        o[name] = row[id].value;
    }
    return o;
};

//this function is the start of it all and calls/references the above functions
initEditor = function() {
    var onDataLoadError, onDataLoadOk, updateEditor;
    if (brushDataList.length <= 0) {
        return;
    }

    xAxisField = brushDataList[0].xAxisField;
    yAxisField = brushDataList[0].yAxisField;
    sheetIndex = brushDataList[0].validSheetIndex;

    onDataLoadError = function(err) {

        return console.error("Error during Tableau Async request:", err._error.message, err._error.stack);
    };

    onDataLoadOk = errorWrapped("Getting data from Tableau", function(table) {
        var col_indexes, data, row, tableauData;
        col_indexes = getColumnIndexes(table, [xAxisField, yAxisField]);
		
        data = (function() {
            var j, len, ref, results;
            ref = table.data;
            results = [];

            var resultsObj = {};

            for (j = 0, len = ref.length; j < len; j++) {
                row = ref[j];
                var convertedRow = convertRowToObject(row, col_indexes);

                // aggregate by date
                if (!resultsObj.hasOwnProperty(convertedRow[xAxisField])) {
                    resultsObj[convertedRow[xAxisField]] = convertedRow;
                } else {
                    resultsObj[convertedRow[xAxisField]].priceee += convertedRow[yAxisField];
                }
            }
            // convert aggregated obj to array
            for (res in resultsObj) {
                results.push(resultsObj[res]);
            }

            // sort by date
            results.sort( (a,b) => {
                if (a[xAxisField] > b[xAxisField]) return 1;
            return -1;
        });

            return results;
        })();

        tableauData = data;

        drawBrush(tableauData); // set up base graph with all data
    });

	//Resets selected field's filter to default value
	tableau.extensions.dashboardContent.dashboard.worksheets[sheetIndex].clearFilterAsync(xAxisField);
	
    tableau.extensions.dashboardContent.dashboard.worksheets[sheetIndex].getSummaryDataAsync({
        maxRows: 0,
        ignoreSelection: true,
        includeAllColumns: true,
        ignoreAliases: true
    }).then(onDataLoadOk, onDataLoadError);

};


this.appApi = {
    initEditor: initEditor
};
